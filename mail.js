require('dotenv').config();
const nodemailer = require('nodemailer');
const mailgun = require('nodemailer-mailgun-transport');

//! configuracion MAILGUN
const auth = {
    auth: {
        api_key: process.env.API_KEY,
        domain: process.env.DOMAIN,
    }
};

//! Middleware
const transporter = nodemailer.createTransport(mailgun(auth));

//! CONFIGURACION PRINCIPAL MAIL
const sendMail = (subject, text, email, cb) => {
    const mailOptions = {
        from: email,
        to: process.env.TO,
        subject: subject + '' +' Te ha enviado una cotizacion',
        html: '<br><h1>Cliente: </h1>' + subject + '<br><h1>Cliente: </h1>' + text + ' <br><h1>EMAIL: </h1>' + email ,
        
    };
    transporter.sendMail(mailOptions, function (err, data) {
        if (err) {
            cb(err, null)
        } else {
            cb(null, data);
        }
    });
}
module.exports = sendMail;

