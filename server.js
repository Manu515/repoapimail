require('dotenv').config();
const express = require('express');
const PORT = 8090;
const app = express();
const path = require('path');
const sendMail = require('./mail.js');

//! configuracion de archivos node
app.use(express.static(__dirname + '/views/public'));
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/public', 'index.html'));
});


app.use(express.urlencoded({
    extended: false
}));



app.use(express.json());
  app.post('/email', (req, res) => {
    

    //! funcion principal / envio

    const { subject, text, email } = req.body;
     console.log('data: ', req.body); 
    sendMail(subject, text, email, function (err, data) {
        if (err) {
            res.status(500).json({ message: 'Internal server error' });
        } else {
            console.log('email enviado');
            res.json({ message: 'Email sent' });
        }
        process.once("SIGHUP", function () {
            reloadSomeConfiguration();
        });
    });
});  

app.listen(PORT, () => {
    console.log('Server runing on:' + PORT);
});